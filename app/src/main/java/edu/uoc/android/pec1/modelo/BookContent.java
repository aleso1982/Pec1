package edu.uoc.android.pec1.modelo;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.uoc.android.pec1.R;

public class BookContent {

    public static final List<BookItem> ITEMS = new ArrayList<>();

    public static class BookItem {
        public final int id;
        public final String title;
        public final String author;
        public final Date publicationDate;
        public final String description;
        public final String coverUrl;

        public BookItem(int id, String title, String author, Date publicationDate, String description, String coverUrl) {
            this.id = id;
            this.title = title;
            this.author = author;
            this.publicationDate = publicationDate;
            this.description = description;
            this.coverUrl = coverUrl;
        }

    }

    static {

        int index = 0;

        Uri coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._0);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._1);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._2);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._3);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._4);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._5);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._6);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._7);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._8);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));

        coverUrl = Uri.parse("android.resource://edu.uoc.android.pec1/" + R.drawable._9);
        ITEMS.add(new BookItem(index , "Title" + ++index, "Author" + index , new Date(), "Description " + index, coverUrl.toString()));
    }
}
