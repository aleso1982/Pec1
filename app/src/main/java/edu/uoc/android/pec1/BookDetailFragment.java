package edu.uoc.android.pec1;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import edu.uoc.android.pec1.modelo.BookContent;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class BookDetailFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private int position;
    private BookContent.BookItem mItem;


    public BookDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey("position")) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = BookContent.ITEMS.get(getArguments().getInt("position"));
        }

        // Inserto título a la appBar
        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(mItem.title);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /* Primero inflamos la vista del Fragment para que podamos acceder a los elementos propios
        del layout donde quisieras mostrar los datos que le llegan al Fragment.
        */

        View rootView = inflater.inflate(R.layout.fragment_book_detail, container, false);

        // Obtenemos la posición del item seleccionado
        Object index = getArguments().get("position");
        position = index.hashCode();

        // Si hay adtos, los inyecto en el view
        if (mItemIsNotNull()) {
            ((TextView) rootView.findViewById(R.id.book_fragment_author)).setText(mItem.author);

            SimpleDateFormat datef = new SimpleDateFormat("dd/MM/yyyy");
            ((TextView) rootView.findViewById(R.id.book_fragment_date)).setText(datef.format(BookContent.ITEMS.get(position).publicationDate));

            ((TextView) rootView.findViewById(R.id.book__fragment_description)).setText(mItem.description);
        }

        return rootView;
    }

    private boolean mItemIsNotNull() {
        return mItem != null;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
