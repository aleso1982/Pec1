package edu.uoc.android.pec1.Vista;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import edu.uoc.android.pec1.BookDetailActivity;
import edu.uoc.android.pec1.R;
import edu.uoc.android.pec1.modelo.BookContent;

// Adaptador que controla la vista inicial de libros para teléfono

public class PhoneRecyclerViewAdapter extends RecyclerView.Adapter<PhoneRecyclerViewAdapter.SolventViewHolders> {

    private final static int EVEN = 0 ;
    private final static int ODD = 1 ;

    private List<BookContent.BookItem> mValues;
    private Context context;
    // Posición del item pulsado sobre el view
    private int currentPos;
    // Almacena la posición del item q se va inyectando en el view
    private int viewTypePosition;

    public PhoneRecyclerViewAdapter(Context context, List<BookContent.BookItem> itemList) {
        this.mValues = itemList;
        this.context = context;
        viewTypePosition = 0;
    }

    @Override
    public SolventViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        SolventViewHolders view = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (getItemViewType(viewType)) {
            case EVEN:
                View v1 = inflater.inflate(R.layout.even_item, parent, false);
                view = new SolventViewHolders(v1);
                break;
            case ODD:
                View v2 = inflater.inflate(R.layout.odd_item, parent, false);
                view = new SolventViewHolders(v2);
                break;
        }

        return  view;
    }

    @Override
    public int getItemViewType ( int position ) {
        int type;
        if (position % 2 == 0) {
            type = EVEN;
        } else type = ODD;
        return type ;
    }

    @Override
    public void onBindViewHolder(final SolventViewHolders holder, int position) {
        // Infla el título y autor de cada uno de los items
        String title = mValues.get(position).title;
        holder.mTitleView.setText(mValues.get(position).title);

        String author = mValues.get(position).author;
        holder.mAuthorView.setText(mValues.get(position).author);
        // obtiene la posición del item;

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ============ INICIO CODIGO A COMPLETAR ===============
                // Obtener la posición del libro donde se ha hecho el click de la vista
                currentPos = holder.getLayoutPosition();

                startPhoneDetailActivity(currentPos);
            }
        });
    }

    private void startPhoneDetailActivity(long currentPos) {
        Intent intent = new Intent(context.getApplicationContext(), BookDetailActivity.class);
        intent.putExtra("position", currentPos);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return this.mValues.size();
    }

    public class SolventViewHolders extends RecyclerView.ViewHolder {

        public final View mView;
        public TextView mTitleView, mAuthorView;
        public BookContent.BookItem mItem;

        public SolventViewHolders(View itemView) {
            super(itemView);
            mView = itemView;
            formattingTitleTextView(itemView);
            formattingAuthorTextView(itemView);
            setBackgroundImageToItemBook(itemView);
        }

        private void formattingAuthorTextView(View itemView) {
            mAuthorView = (TextView) itemView.findViewById(R.id.book_author);
            mAuthorView.setTextColor(context.getResources().getColor(R.color.colorAccent));
            mAuthorView.setTypeface(null, Typeface.BOLD);
        }

        private void formattingTitleTextView(View itemView) {
            mTitleView = (TextView) itemView.findViewById(R.id.book_title);
            mTitleView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            mTitleView.setTextSize(20);
            mTitleView.setTypeface(null, Typeface.BOLD);
        }
    }

    private void setBackgroundImageToItemBook(View itemView) {
        itemView.findViewById(R.id.book_card_view_layout).setBackgroundColor(Color.WHITE);
        ImageView coverImage = (ImageView) itemView.findViewById(R.id.book_layout);
        // Uso la libreria Picasso para ponerle fondo a la imagen desde una URL
        Picasso.with(context).load(mValues.get(viewTypePosition++).coverUrl).into((coverImage));
    }
}
