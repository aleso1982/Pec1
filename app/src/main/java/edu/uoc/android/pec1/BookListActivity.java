package edu.uoc.android.pec1;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import edu.uoc.android.pec1.Vista.PhoneRecyclerViewAdapter;
import edu.uoc.android.pec1.Vista.SimpleItemRecyclerViewAdapter;
import edu.uoc.android.pec1.modelo.BookContent;

public class BookListActivity extends AppCompatActivity implements BookDetailFragment.OnFragmentInteractionListener {

    private ArrayList<String> itemList;

    // Determina en que dispositivo está corriendo la app
    public static boolean mTwoPane;

    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private CoordinatorLayout coordinatorBokkListLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        coordinatorBokkListLayout = (CoordinatorLayout) findViewById(R.id.cc);

        initializeActionBar();

        initializeFloatingButton();

        initializeComponents();

        if (findViewById(R.id.book_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
            coordinatorBokkListLayout.findViewById(R.id.degraded_image).setVisibility(View.INVISIBLE);
        } else {
            initializePhoneView();
        }

        initializeComponents();
        
    }

    private void initializePhoneView() {
        mTwoPane = false;
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(-1)) {
                    onScrolledToTop();
                } else if (!recyclerView.canScrollVertically(1)) {
                    onScrolledToBottom();
                } else if (dy < 0) {
                    onScrolledUp();
                } else if (dy > 0) {
                    onScrolledDown();
                }
            }

            public void onScrolledUp() {
                Log.d("onScrolledUp", "onScrolledUp");
                coordinatorBokkListLayout.findViewById(R.id.degraded_image).setVisibility(View.VISIBLE);
            }

            public void onScrolledDown() {
                Log.d("onScrolledUp", "onScrolledUp");
            }

            public void onScrolledToTop() {
                Log.d("onScrolledToTop", "onScrolledToTop");
            }

            public void onScrolledToBottom() {
                Log.d("onScrolledToBottom", "onScrolledToBottom");
                coordinatorBokkListLayout.findViewById(R.id.degraded_image).setVisibility(View.INVISIBLE);
            }
        });
    }

    private void initializeFloatingButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Nothing to do", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void initializeActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    private void initializeComponents() {
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);

        if (mTwoPane) {
            SimpleItemRecyclerViewAdapter adapter = new SimpleItemRecyclerViewAdapter(this, BookContent.ITEMS);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        } else {

            recyclerView.setHasFixedSize(true);
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gaggeredGridLayoutManager);

            PhoneRecyclerViewAdapter adapter = new PhoneRecyclerViewAdapter(this, BookContent.ITEMS);
            recyclerView.setAdapter(adapter);
        }
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //int orientation = this.getResources().getConfiguration().orientation;

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d("Portrait ","PPPPPPPPPPPPPPPPP");
            //setContentView(R.layout.settings);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d("LandScape ","LLLLLLLLLLLLLLLLLLLL");
            //setContentView(R.layout.settings);
        }

    }

}
