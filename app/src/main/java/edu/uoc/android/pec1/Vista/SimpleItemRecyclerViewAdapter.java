package edu.uoc.android.pec1.Vista;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import edu.uoc.android.pec1.BookDetailFragment;
import edu.uoc.android.pec1.R;
import edu.uoc.android.pec1.modelo.BookContent;

// Adaptador que controla la vista inicial de libros para tablet
public class SimpleItemRecyclerViewAdapter
        extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

    // Lista de libros
    private final List<BookContent.BookItem> mValues;
    private AppCompatActivity context;

    // Identifica si el layout es par o impar
    private final static int EVEN = 0 ;
    private final static int ODD = 1 ;

    public SimpleItemRecyclerViewAdapter(AppCompatActivity context, List<BookContent.BookItem> items) {
        this.context = context;
        this.mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // ============ INICIO CODIGO A COMPLETAR ===============
        ViewHolder view = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (getItemViewType(viewType)) {
            case EVEN:
                View v1 = inflater.inflate(R.layout.even_item, parent, false);
                view = new ViewHolder(v1);
                break;
            case ODD:
                View v2 = inflater.inflate(R.layout.odd_item, parent, false);
                view = new ViewHolder(v2);
                break;
        }
        // ============ FIN CODIGO A COMPLETAR =================
        return view;
    }

    @Override
    public int getItemViewType ( int position ) {
        int type;
        if (position % 2 == 0) {
            type = EVEN;
        } else type = ODD;
        return type ;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // ============ INICIO CODIGO A COMPLETAR ===============
        // Guarda la posición actual en la vista del holder
        holder.mItem = mValues.get(position);

        // Infla el título y autor de cada uno de los items
        holder.mTitleView.setText(mValues.get(position).title);
        holder.mAuthorView.setText(mValues.get(position).author);

        // ============ FIN CODIGO A COMPLETAR =================
        holder.mView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPos;
                // ============ INICIO CODIGO A COMPLETAR ===============
                // Obtener la posición del libro donde se ha hecho el click de la vista
                currentPos = holder.getLayoutPosition(); // obtiene la posición del item;
                // ============ FIN CODIGO A COMPLETAR =================

                // ============ INICIO CODIGO A COMPLETAR ===============
                // Iniciar el fragmento correspondiente a tableta, enviando el argumento
                // de la posición seleccionada
                BookDetailFragment fragment = new BookDetailFragment();
                Bundle args = new Bundle();
                args.putInt("position", currentPos);
                fragment.setArguments(args);
                context.getSupportFragmentManager().beginTransaction().replace(R.id.book_detail_container, fragment).commit();

                /*if (BookListActivity.mTwoPane) {
                    // ============ INICIO CODIGO A COMPLETAR ===============
                    // Iniciar el fragmento correspondiente a tableta, enviando el argumento
                    // de la posición seleccionada
                    BookDetailFragment fragment = new BookDetailFragment();
                    Bundle args = new Bundle();
                    args.putInt("position", currentPos);
                    fragment.setArguments(args);
                    context.getSupportFragmentManager().beginTransaction().replace(R.id.book_detail_container, fragment).commit();
                    // ============ FIN CODIGO A COMPLETAR =================
                } else {
                    // ============ INICIO CODIGO A COMPLETAR ===============
                    // Iniciar la actividad correspondiente a móvil, enviando el argumento de la posición seleccionada
                    Intent intent = new Intent(context.getApplicationContext(), BookDetailActivity.class);
                    intent.putExtra("position", currentPos);
                    context.startActivity(intent);
                    // ============ FIN CODIGO A COMPLETAR =================
                }*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleView;
        public final TextView mAuthorView;
        public BookContent.BookItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitleView = (TextView) view.findViewById(R.id.book_title);
            mAuthorView = (TextView) view.findViewById(R.id.book_author);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mAuthorView.getText() + "'";
        }
    }
}
