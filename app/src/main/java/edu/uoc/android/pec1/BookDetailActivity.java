package edu.uoc.android.pec1;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class BookDetailActivity extends AppCompatActivity implements BookDetailFragment.OnFragmentInteractionListener {

    TextView bookTitle, bookAuthor, bookDate, bookDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_detail_activity);

        initializeActionBar();

        initializeFloatingButton();

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle args = getArguments();
            int position = getItemBookPosition(args);
            putArguments(args, position);

            BookDetailFragment fragment = createFragment(args);

            executeFragment(fragment);
        }
    }

    private void initializeFloatingButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Nothing to do", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void initializeActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void executeFragment(BookDetailFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.book_detail_phone, fragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    @NonNull
    private BookDetailFragment createFragment(Bundle args) {
        BookDetailFragment fragment = new BookDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void putArguments(Bundle args, int position) {
        args.putInt("position", position);
    }

    private int getItemBookPosition(Bundle args) {
        Object index = args.get("position");
        return index.hashCode();
    }

    private Bundle getArguments() {
        return this.getIntent().getExtras();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
